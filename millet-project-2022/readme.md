# Millet
## 开发工具使用
推荐使用VScode，同时安装EditorConfig for VS Code。

当然亦可使用其他编辑器，但希望能够支持EditorConfig。

可在[其官网](https://editorconfig.org/)查看支持EditorConfig的编辑器。

环境配置请阅读[开发环境配置文档](./doc/backend/1.Development%20Environment.md)

## 小程序介绍文档

本链接包含了每次迭代的视频及功能介绍

https://nankai.feishu.cn/docs/doccnEGMvegabw3VAyCDCALDwrI
